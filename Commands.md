
### Show leader node (in annotatios)
 kubectl get endpoints kube-controller-manager --namespace=kube-system  -o yaml


Quickly retrieve imperative commands to create k8s resources.
Command: k run -h | grep '# ' -A2

Determine proper api_group/version for a resource
Command1: k api-resources | grep -i "resource name"

Command2: k api-versions | grep -i "api_group name"


Quickly find kube api server setting
Command1: ps -ef --forest | grep kube-apiserver | grep "<SETTING>"

Switch to namespace as default
Command: kubectl config set-context --current --namespace=new namespace

Get help for different k8s resources
Command: kubectl explain pods.spec.containers | less

Command variation 1: kubectl explain pods.spec.containers --recursive | less

Display all k8s resources
Command: kubectl api-resources -owide


Use busybox for running utilities
Busybox page

show
Command: kubectl run -it --rm debug --image=busybox --restart=Never -- sh
kubectl run -it --rm debug --image=radial/busyboxplus:curl --restart=Never -- curl http://servicename

cat myuser.csr | base64 | tr -d "\n"


Retrieve token from secret to access dashboard
Command:

kubectl -n kubernetes-dashboard get secret \
$(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") \
-o go-template="{{.data.token | base64decode}}"

Identify field in a certificate
Command: openssl x509 -noout -subject -in /path/cert.crt Result: This command outputs subject (CN) or a certificate, other fields are available in help

Alternative Command: openssl x509 -in /path/cert.file -text Result: This command outputs all information about certificate


Quick Delete Pod
kubectl delete po <pod name> <optional -n namespace> --wait=false

netstat -plnt

ss -lp



