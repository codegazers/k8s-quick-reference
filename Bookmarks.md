            https://separator.mayastudios.com/ -- -------------------- YAML -------------------
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/audit/audit-policy.yaml -- AUDITPOLICY: auditPolicy spec
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/application/job/cronjob.yaml -- CRONJOB: cronjbo spec
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/application/simple_deployment.yaml -- DEPLOY: sample nginx deployment spec
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/service/networking/minimal-ingress.yaml -- INGRESS: ingress spec
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/service/networking/nginx-policy.yaml -- NETWORKPOLICY: spec
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-configmap-envFrom.yaml -- POD: configMap as env
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-configmap-envFrom.yaml -- POD: env as configMapRef [all values]
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-single-configmap-env-variable.yaml -- POD: env as configMapKeyRef [single value]
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/inject/envars.yaml -- POD: inline environmental variables
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/probe/exec-liveness.yaml -- POD: livenessProbe [command]
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/probe/http-liveness.yaml -- POD: livenessProbe [http]
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/pods/private-reg-pod.yaml -- POD: imagePullSecrets
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-with-node-affinity.yaml -- POD: nodeAffinity
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-nginx.yaml -- POD: nodeSelector
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/commands.yaml -- POD: command and args
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/inject/pod-secret-envFrom.yaml -- POD: env from secretRef
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-projected-svc-token.yaml -- POD: projected volume [sa token]
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/pods/storage/pv-pod.yaml -- POD: persistentVolumeClaim
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/pod-with-toleration.yaml -- POD: tolerations
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/pods/storage/redis.yaml -- POD: volume mount [emptyDir]
            https://kubernetes.io/docs/concepts/storage/volumes/#emptydir-configuration-example -- POD: volume mount [hostPath]
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/pods/security/security-context.yaml -- POD: securityContext [basic config]
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/pods/storage/pv-volume.yaml -- PV: pv spec
            https://raw.githubusercontent.com/kubernetes/website/main/content/en/examples/pods/storage/pv-claim.yaml -- PVC: pvc spec
            https://kubernetes.io/docs/concepts/containers/runtime-class/#2-create-the-corresponding-runtimeclass-resources -- RUNTIME-CLASS: spec
            https://kubernetes.io/docs/concepts/services-networking/service/#nodeport -- SERVICE: nodePort
            https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer -- SERVICE: loadBlancer
            https://separator.mayastudios.com/ -- ---------------- TASKS ----------------
            https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#imagepolicywebhook -- ADMISSION-CONTROLLER:  configure ImagePolicyWebhook 
            https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/ -- CLUSTER: installation
            https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/ -- CLUSTER:  upgrade
            https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/#create-a-certificate-signing-request-object-to-send-to-the-kubernetes-api -- CSR: create and execute
            https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/#deploying-the-dashboard-ui -- DASHBOARD: deploy dashboard
            https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#updating-a-deployment -- DEPLOY: update deployment and set new image
            https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/#create-a-simple-pod-to-use-as-a-test-environment -- DNS: debug DNS
            https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/#create-a-simple-pod-to-use-as-a-test-environment -- DNS: test resolution
            https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster -- ETCD: create backup
            https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/ -- KUBELET: config options
            https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#which-plugins-are-enabled-by-default -- KUBE-API: admissionPlugins [default admission plugins]
            https://kubernetes.io/docs/tasks/administer-cluster/declare-network-policy/#limit-access-to-the-nginx-service -- NETPOL: limit ingress
            https://kubernetes.io/docs/concepts/services-networking/network-policies/#behavior-of-to-and-from-selectors -- NETPOL: namespace selector
            https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/#concepts -- NODE: taint node to be unschedulable
            https://kubernetes.io/docs/concepts/containers/images/#referring-to-an-imagepullsecrets-on-a-pod -- POD: configure pulling images from private repository
            https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-memory -- POD: configure resources limits
            https://kubernetes.io/docs/concepts/cluster-administration/logging/#sidecar-container-with-a-logging-agent -- POD: configure sidecar container with logging agent
            https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#init-containers-in-use -- POD: configure initContainer
            https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-example -- ROLE: create via YAML
            https://kubernetes.io/docs/reference/access-authn-authz/rbac/#command-line-utilities -- ROLE: create imperatively
            https://kubernetes.io/docs/reference/access-authn-authz/rbac/#kubectl-create-rolebinding -- ROLEBINDING: create imperatively
            https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-and-clusterrole -- ROLEBINGIND: creating declaratively
            https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/#decoding-secret -- SECRET: decode secret values
            https://separator.mayastudios.com/ -- ---------------- CKS ONLY ----------------
            https://gitlab.com/apparmor/apparmor/-/wikis/Documentation -- APPARMOR: Documentation
            https://kubernetes.io/docs/tutorials/clusters/apparmor/#securing-a-pod -- APPARMOR: restrict a container&#39;s access
            https://kubernetes.io/docs/tasks/debug-application-cluster/audit/#audit-policy -- AUDITPOLICY: implement audit policy
            https://falco.org/docs/rules/supported-fields/ -- FALCO: output fields
            https://github.com/falcosecurity/falco/blob/master/rules/falco_rules.yaml -- FALCO: default rules
            https://kubernetes.io/docs/tasks/tls/certificate-rotation/#enabling-client-certificate-rotation -- KUBELET: enable certificates Rotation for the Kubelet 
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/policy/example-psp.yaml -- PSP: podSecurityPolicy resource
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/security/security-context-4.yaml -- POD: securityContext [capabilities]
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/security/security-context-4.yaml -- POD: securityContext [container level add capabilities]
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/security/security-context-2.yaml -- POD: securityContext  [container level runAs]
            https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/pods/security/security-context.yaml -- POD: securityContext [pod level runAs]
            https://kubernetes.io/docs/concepts/containers/runtime-class/#2-create-the-corresponding-runtimeclass-resources -- RUNTIMECLASS: usage
            https://kubernetes.io/docs/tutorials/clusters/seccomp/#create-pod-with-seccomp-profile-that-only-allows-necessary-syscalls -- SECCOMP: seccompProfile [restrict pod]
            https://docs.sysdig.com/?lang=en -- SYSDIG: documentation
            https://github.com/aquasecurity/trivy -- TRIVY: documentation
 
