
### Show resources associated to namespaces
```
kubectl api-resources --namespaced
```

### Show Cluster-wide resources
```
kubectl api-resources --namespaced=false
```
