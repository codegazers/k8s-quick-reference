
### - Quickly create a "sleep" busybox Pod.
```
$ kubectl run pod --image=busybox --command -o yaml --dry-run=client >pod.yaml -- sh -c 'sleep 1d' 

$ cat pod.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod
  name: pod
spec:
  containers:
  - command:
    - sh
    - -c
    - sleep 1d
    image: busybox
    name: pod
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
