# Helm Quick Reference

### Add/Update Repository
```
helm repo add nvdp https://nvidia.github.io/k8s-device-plugin

helm repo update
```

### Search for chart and all versions available
```
helm search repo bitnami/mysql --versions
```


### Download official chart to see implementation and values
```
helm pull bitnami/mysql --version 8.5.8 . --untar
```

### View official chart info
```
helm show chart bitnami/redis

helm show values bitnami/redis
```

### Install release from official chart with version
```
helm upgrade mysql bitnami/mysql --version 8.5.8 -f values-uat01.yaml --install
```

This will create a release called mysql from official bitnami mysql chart with version 8.5.8
-f values-uat01.yaml assume you have modify some of the values and supply it with this file. you can also use --set name=value to supply just individual value as opposed to the whole file if you want.

### Install or upgrade a chart (run in chart directory)
```
helm upgrade test . -f values-uat01.yaml -n namespace --install
```

In this case, chart is already downloaded in current directory.


### Install or upgrade a chart with dry run (run in chart directory)
```
helm upgrade test . -f values-uat01.yaml -n namespace --install --dry-run
```

### Render the template locallly
```
helm template test . -f values-uat01.yaml
```

This will output what will actually be rendered from your char

### Upgrade Existing Release with new values
```
helm upgrade --reuse-values --set image.registry=docker.io/bitnami/redis --set image.tag=6.0.7-debian-10-r0 redis stable/redis --version 3.2.5 --dry-run
```

Great command beacuse it will reuse current values and only change specific ones.

### Uninstall a Release
```
helm uninstall test
```

### Rollback to previous release
```
helm rollback test
```

### List releases
```
helm ls

helm ls -n namespace

helm list -A
```

### Show information about current release

- Values
```
helm get values test
```

- All deployed k8s objects
```
helm get manifest test
```

- All available information (chart name, manifest, values and notes)
```
helm get all test
```
